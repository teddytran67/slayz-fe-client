/* eslint-disable react-hooks/exhaustive-deps */
import { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import "../../index.css";
import logo from "../../assets/slay_logo_light.png";
import { FiSearch } from "@react-icons/all-files/fi/FiSearch";
import { HiMenu } from "@react-icons/all-files/hi/HiMenu";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiShoppingCart } from "@react-icons/all-files/hi/HiShoppingCart";
import { useEffect } from "react";
import { useRef } from "react";
import Swal from "sweetalert2";
import { Dropdown } from "flowbite-react";

const Header = () => {
  const navigate = useNavigate();
  const inputRef = useRef(null);
  const { authState, getCurrentUser } = useContext(AuthContext);
  const searchOnClick = (e) => {
    e.preventDefault();
    if (inputRef.current.value.trim() !== "") {
      navigate("/products/search?q=" + inputRef.current.value);
      inputRef.current.value = "";
    } else {
      Swal.fire("Hãy nhập từ khóa tìm kiếm!");
      inputRef.current.value = "";
      inputRef.current.focus();
    }
  };
  useEffect(() => {
    getCurrentUser();
  }, []);
  return (
    <div className="flex flex-col justify-center items-center z-[10000] fixed w-full top-0 left-0 xl:backdrop-blur-xl shadow-md hover:shadow-lg bg-main-bg backdrop-blur-none xl:bg-white box-border">
      <div className="px-3 py-4 w-full xl:h-[70px] h-[60px] flex justify-start items-center transition-all">
        <div className="flex xl:hidden justify-between w-full sticky h-[70px] items-center">
          <div className="justify-center items-center flex">
            <img
              className="cursor-pointer h-[40px] object-cover hover:scale-110 transition-all duration-300"
              src={logo}
              alt="logo"
              onClick={() => {
                navigate("/");
              }}
            />
          </div>
          <div className="">
            <Dropdown
              label={
                <div className="">
                  <HiMenu className="text-[30px] text-white" />
                </div>
              }
            >
              <form onSubmit={searchOnClick}>
                <div className="flex justify-center items-center border-b border-gray-600">
                  <FiSearch
                    onClick={searchOnClick}
                    className="text-black text-3xl hover:rounded-full hover:bg-gray-300 hover:text-white p-1 transition-all"
                  />
                  <input
                    className="bg-transparent p-1 text-black placeholder:text-gray-600 border-none focus:ring-0"
                    type="text"
                    placeholder="Tìm kiếm"
                    ref={inputRef}
                  />
                </div>
              </form>
              <Dropdown.Divider />
              <Dropdown.Item>
                <div className="w-[200px]">
                  <Link to="/products/male">Sản phẩm dành cho Nam</Link>
                </div>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/products/female">Sản phẩm dành cho Nữ</Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/products/unisex">Sản phẩm Unisex</Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/products">Tất cả sẩn phẩm</Link>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item>
                {authState.isAuthenticated === true ? (
                  <div className="w-full flex gap-2 font-semibold text-lg flex-col justify-center items-center">
                    <div className="w-full flex justify-start items-center">
                      <HiUser />
                      <button
                        className="transition-all duration-200 hover:scale-105 mt-1 ml-2"
                        onClick={() => {
                          navigate("/account");
                        }}
                      >
                        Thông tin cá nhân
                      </button>
                    </div>
                    <div className="w-full flex justify-start items-center">
                      <HiShoppingCart />
                      <Link
                        className="transition-all duration-200 hover:scale-105 font-semibold text-lg ml-2"
                        to="/user/cart"
                      >
                        Giỏ hàng
                      </Link>
                    </div>
                  </div>
                ) : (
                  <Link
                    className="transition-all duration-200 hover:scale-105 font-semibold text-lg"
                    to="/login"
                  >
                    Đăng nhập
                  </Link>
                )}
              </Dropdown.Item>
            </Dropdown>
          </div>
        </div>
        <div className="hidden xl:flex justify-between w-full sticky h-[70px]">
          <ul className="flex justify-center items-center gap-6 ml-4 font-semibold text-md">
            <li className="">
              <Link to="/">
                <img
                  className="cursor-pointer h-[40px] object-cover hover:scale-110 transition-all duration-300"
                  src={logo}
                  alt="logo"
                />
              </Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 mt-1">
              <Link to="/products/male">NAM</Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 mt-1">
              <Link to="/products/female">NỮ</Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 mt-1">
              <Link to="/products/unisex">UNISEX</Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 text-primary-color mt-1">
              <Link to="/products">TẤT CẢ SẢN PHẨM</Link>
            </li>
          </ul>
          <div className="flex justify-center items-center gap-6">
            <div className="flex justify-start items-center border-b border-gray-600 w-[400px]">
              <FiSearch
                onClick={searchOnClick}
                className="text-black text-3xl hover:rounded-full hover:bg-gray-300 hover:text-white p-1 transition-all"
              />
              <input
                className="bg-transparent p-1 text-black placeholder:text-gray-600 border-none focus:ring-0 w-full"
                type="text"
                placeholder="Tìm kiếm"
                ref={inputRef}
              />
            </div>
            {authState.isAuthenticated === true ? (
              <div className="flex gap-6 font-semibold text-lg mt-2">
                <button
                  className="transition-all duration-200 hover:scale-105 flex justify-center items-center gap-1"
                  onClick={() => {
                    navigate("/account");
                  }}
                >
                  <HiUser />
                  <p className="mt-[1px]">TÀI KHOẢN</p>
                </button>
                <Link
                  className="transition-all duration-200 hover:scale-105 font-semibold text-lg flex justify-center items-center gap-1"
                  to="/user/cart"
                >
                  <HiShoppingCart />
                  <p className="mt-[1px]">GIỎ HÀNG</p>
                </Link>
              </div>
            ) : (
              <Link
                className="transition-all duration-200 hover:scale-105 font-semibold text-lg"
                to="/login"
              >
                ĐĂNG NHẬP
              </Link>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default Header;
