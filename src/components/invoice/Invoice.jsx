import React from "react";
import { useNavigate } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";

const Invoice = (props) => {
  const navigate = useNavigate();
  const statusDisplay = (status) => {
    if (status === "PENDING") {
      return "Đã đặt hàng";
    } else if (status === "CANCELLED") {
      return "Đã hủy";
    } else if ("COMPLETE") {
      return "Đã nhận hàng";
    } else {
      return "Đã tiếp nhận";
    }
  };
  return (
    <div className="h-fit w-full p-4 border border-gray-500 flex">
      <div className="w-4/5">
        <div>
          <p className="font-bold">
            {props.fullName} - {props.phone}
          </p>
        </div>
        <div>
          <p>{props.address}</p>
        </div>
        <div>
          <p>Trạng thái: {statusDisplay(props.status)}</p>
        </div>
        <div>
          <p>Vận chuyển: {props.delivery}</p>
        </div>
        <div>
          <p>Ngày đặt hàng: {props.date}</p>
        </div>
        <div>
          <p>Tổng: {GlobalUtil.numberWithCommas(props.total)} VNĐ</p>
        </div>
      </div>
      <div className="w-1/5 flex justify-center items-center">
        <button
          className="p-4 py-2 bg-black text-white"
          type="button"
          onClick={() => {
            navigate("/account/bill/" + props.id);
          }}
        >
          CHI TIẾT
        </button>
      </div>
    </div>
  );
};

export default Invoice;
