import React from "react";

const Alert = (props) => {
	if (props.show === false) {
		return <div></div>;
	}
	return (
		<div className={`w-full py-2 px-3 flex gap-2 bg-[${props.color}]`}>
			<div>{props.icon}</div>
			<div className="text-black">{props.content}</div>
		</div>
	);
};

export default Alert;
