import { ProductGallery } from "../components";
import banner1 from "../assets/HomePage1.png";
import banner2 from "../assets/banner_2.jpg";
import banner4 from "../assets/banner_4.jpg";
import banner5 from "../assets/banner_5.jpg";
import banner6 from "../assets/banner_6.jpg";
import category_1 from "../assets/3Cate1.png";
import category_2 from "../assets/3Cate2.png";
import category_3 from "../assets/3Cate3.png";
import { useState } from "react";
import { ProductService } from "../services/product.service";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Carousel } from "flowbite-react";

const categoryList = [
  {
    id: 0,
    category: "NAM",
    quantity: 30,
    image: category_1,
  },
  {
    id: 1,
    category: "NỮ",
    quantity: 50,
    image: category_2,
  },
  {
    id: 2,
    category: "UNISEX",
    quantity: 20,
    image: category_3,
  },
];

const Home = () => {
  const [productData, setProductData] = useState([]);
  const navigate = useNavigate();
  const textAblout =
    "Như ý nghĩa của tên gọi, trang phục của SlayZ hướng đến việc trở thành thói quen, lựa chọn hàng ngày cho nam giới trong mọi tình huống. Bởi SlayZ hiểu rằng, sự tự tin về phong cách ăn mặc sẽ làm nền tảng, tạo động lực cổ vũ mỗi người mạnh dạn theo đuổi những điều mình mong muốn. Trong đó, trang phục nam phải mang vẻ đẹp lịch lãm, hợp mốt và tạo sự thoải mái, quan trọng nhất là mang đến cảm giác “được là chính mình” cho người mặc.";
  const peopleTalk =
    "Routine đang tạo ra những bộ trang phục sản xuất trong nước hoàn toàn có thể sánh ngang với các thương hiệu thời trang nam đến từ nước ngoài về kiểu dáng, chất lượng lẫn phong cách thời trang.";
  const fetchData = () => {
    ProductService.get12TheNewestProduct().then((response) => {
      if (response.status === "OK") {
        setProductData(response.data);
      }
    });
  };

  useEffect(() => {
    fetchData();
    window.scrollTo(0, 0);
  }, []);
  return (
    <div className="w-full flex flex-col md:justify-center md:items-center xl:mt-[70px] mt-[60px]">
      <div className="w-full 2xl:h-[900px] xl:h-[800px] lg:h-[700px] md:h-[600px] h-[500px] flex items-start justify-start">
        <Carousel slideInterval={3000}>
          <img
            className="overflow-hidden object-cover w-full h-[500px] xl:h-fit"
            src={banner2}
            alt="banner 1"
          />
          <img
            className="overflow-hidden object-cover w-full h-[500px] xl:h-fit"
            src={banner1}
            alt="banner 1"
          />
          <img
            className="overflow-hidden object-cover w-full h-[500px] xl:h-fit"
            src={banner4}
            alt="banner 1"
          />
          <img
            className="overflow-hidden object-cover w-full h-[500px] xl:h-fit"
            src={banner5}
            alt="banner 1"
          />
          <img
            className="overflow-hidden object-cover w-full h-[500px] xl:h-fit"
            src={banner6}
            alt="banner 1"
          />
        </Carousel>
      </div>
      {/* Danh mục hình ảnh */}
      <div className="relative w-full flex flex-col justify-center items-center">
        <div id="poly-shape-first" className=""></div>
        <div className="md:max-w-[1721px] w-full md:mt-[150px] mt-[60px] py-2 px-3">
          <h1 className="font-bold text-white text-xl">PHÂN LOẠI SẢN PHẨM</h1>
          <p className="text-white text-sm mt-1">Hãy chọn sản phẩm ưa thích dành cho bạn</p>
        </div>
        <div className="flex md:max-w-[1721px] w-full my-3 flex-wrap px-3">
          {categoryList.map((item, index) => {
            return (
              <div key={index} className="md:w-1/3 w-full relative h-fit overflow-hidden bg-black">
                <div className="absolute w-full flex justify-end p-4">
                  <div className="w-fit">
                    <h1 className="text-red-500 font-bold">{item.category}</h1>
                    <p className="text-[#715F5B]">{item.quantity}+ sản phẩm</p>
                  </div>
                </div>
                <img src={item.image} className="w-full" alt="category_photo" />
                <div className="flex justify-center items-center w-full h-full bg-black absolute top-0 left-0 opacity-0 hover:opacity-60 transition-all duration-300">
                  <button
                    type="button"
                    className="p-3 border-white border-2 rounded-md text-white"
                    onClick={() => {
                      if (item.id === 0) {
                        navigate("/products/male");
                      } else if (item.id === 1) {
                        navigate("/products/female");
                      } else {
                        navigate("/products/unisex");
                      }
                    }}
                  >
                    KHÁM PHÁ
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      {/* Sản phẩm nổi bật */}
      <div className="md:max-w-[1721px] mt-10 py-2 md:mx-0 mx-auto w-full">
        <h1 className="font-bold text-[#55595C] text-xl md:text-left text-center">
          SẢN PHẨM MỚI NHẤT
        </h1>
      </div>
      <div className="w-full flex justify-center items-center">
        <div className="md:max-w-[1721px] mx-auto w-full">
          <div className="flex flex-wrap md:items-center md:justify-center md:w-full my-3 md:gap-y-[10px] md:gap-x-[30px] gap-y-[10px] justify-center items-start w-full md:px-0 px-2 gap-x-[10px]">
            {productData.map((item, index) => (
              <ProductGallery {...item} key={index} />
            ))}
          </div>
        </div>
      </div>
      <div className="w-[100%] flex justify-center items-center py-8 my-8 md:w-[76%]">
        <div
          className="cursor-pointer flex justify-center items-center font-semibold text-gray-700 h-[40px] w-[180px] border border-gray-600 hover:text-white hover:bg-black transition-all"
          onClick={() => {
            navigate("/products");
          }}
        >
          KHÁM PHÁ
        </div>
      </div>
      {/* Về chúng tôi */}
      <div className="relative w-[100%] mb-[130px] flex flex-col justify-center items-center">
        <div className="md:w-[76%] w-full flex-wrap flex mb-8 bg-[#EAEFF2] md:flex-row flex-col">
          <div className="md:w-1/2 bg-black w-full">
            <div className="flex opacity-75 w-fit h-full overflow-hidden justify-around">
              {categoryList.map((item) => {
                return (
                  <img
                    src={item.image}
                    key={item.id}
                    alt="not found"
                    className="w-1/3 h-full object-cover"
                  />
                );
              })}
            </div>
          </div>
          <div className="md:w-1/2 w-full pl-2 flex justify-center items-center">
            <div className="md:px-0 md:py-0 px-2 py-3">
              <h1 className="text-primary-color font-bold text-xl px-5 text-center md:text-left">
                VỀ CHÚNG TÔI
              </h1>
              <p className="text-xl text-justify mt-1 leading-10 px-5">{textAblout}</p>
            </div>
          </div>
        </div>
        <div id="poly-shape-second"></div>
        <div className="w-full flex flex-col justify-center items-center h-fit pb-10">
          <h1 className="font-bold text-2xl px-5 my-5 text-center text-primary-color">
            MỌI NGƯỜI NÓI GÌ VỀ CHÚNG TÔI
          </h1>
          <div className="w-full 2xl:h-[500px] xl:h-[400px] lg:h-[300px] md:h-[200px] h-[100px] flex items-center justify-center">
            <div className="flex flex-col justify-center items-center bg-[#E4EAEE] p-8 md:w-[800px] w-full">
              <p className="text-2xl text-center mt-1 leading-10 px-5 mb-5">
                <em>{peopleTalk}</em>
              </p>
              <h1 className="text-primary-color font-bold text-xl text-justify px-5">
                {" "}
                -TRẦN ANH TIẾN-
              </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
