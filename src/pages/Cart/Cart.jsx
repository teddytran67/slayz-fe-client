/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import CartDetail from "./CartDetail";
import { AiOutlineShoppingCart } from "@react-icons/all-files/ai/AiOutlineShoppingCart";
import { useEffect } from "react";
import { CartService } from "../../services/cart.service";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { useDataContext } from "../../contexts/DataProvider";

const Cart = () => {
  const navigate = useNavigate();
  const { cartData, setCartData, fetchCartData, flagChange, setFlagChange } = useDataContext();
  const [cartDetails, setCartDetails] = useState([]);
  const fetchData = () => {
    const userId = JSON.parse(localStorage.getItem("user"))?.data?.userInfo?.id;
    CartService.getCartByUserId(userId).then((response) => {
      if (response.status === "OK") {
        CartService.getCartById(response.data.id).then((response) => {
          setCartData(response.data);
        });
        CartService.getAllCartDetailByCartId(response.data?.id).then((response) => {
          if (response.status === "OK") {
            setCartDetails(response.data);
          }
        });
      } else {
        navigate("/login");
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchData();
  }, [flagChange]);
  if (cartDetails.length === 0) {
    return (
      <div className="mt-[122px] h-[64vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
        <div>
          <AiOutlineShoppingCart className="text-[180px]" />
        </div>
        <div>Chưa sản phẩm nào trong giỏ hàng.</div>
        <button
          className="transition-all duration-200 hover:shadow-md bg-none p-3 w-[20%] text-black border border-gray-600"
          type="button"
          onClick={() => {
            navigate("/");
          }}
        >
          QUAY LẠI MUA SẮM
        </button>
      </div>
    );
  }

  return (
    <div className="h-fit w-[80%] mx-auto flex flex-col justify-center items-center mt-[142px]">
      <div className="w-full">
        <h1 className="font-bold">GIỎ HÀNG</h1>
        <p>{cartData?.quantityProduct} sản phẩm</p>
      </div>
      {/* Danh sách sản phẩm */}
      <div className="w-full flex">
        {/* Sản phẩm */}
        <div className="flex flex-col divide-y-2 gap-[40px] py-4 w-2/3 mb-[150px]">
          {cartDetails.map((item) => {
            return <CartDetail {...item} key={item?.id} />;
          })}
          {cartDetails.length !== 0 && (
            <div type="button" className="w-full py-3">
              <button
                className="transition-all w-full py-3 bg-black text-white hover:bg-gray-700"
                onClick={() => {
                  fetchCartData();
                  setFlagChange((prev) => {
                    return !prev;
                  });
                  window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
                }}
              >
                CẬP NHẬT GIỎ HÀNG
              </button>
            </div>
          )}
        </div>
        {/* Chi tiết */}
        <div className="w-1/3 flex flex-col items-end justify-start gap-5">
          <div className="p-6 px-[40px] w-[80%] flex flex-col gap-5 border border-gray-500">
            <div className="">
              <p className="font-bold uppercase">TẠM TÍNH</p>
            </div>
            <div className="flex justify-between items-center">
              <p>Số lượng</p>
              <p>{cartData?.totalQuantity}</p>
            </div>
            <div className="flex justify-between items-center">
              <p>Tạm tính</p>
              <p>{GlobalUtil.numberWithCommas(cartData?.grandTotal)} ₫</p>
            </div>
            <div className="flex justify-between items-center">
              <p>VAT</p>
              <p>0%</p>
            </div>
            <div className="flex justify-between items-center border-t border-gray-400 pt-6 pb-2">
              <p>Tổng số</p>
              <p className="font-bold uppercase">
                {GlobalUtil.numberWithCommas(cartData?.grandTotal)} ₫
              </p>
            </div>
          </div>
          <button
            className="transition-all duration-200 hover:shadow-md bg-blue-600 hover:bg-blue-700 text-white p-3 w-[80%]"
            type="button"
            onClick={() => {
              navigate("/checkout/address");
            }}
          >
            THANH TOÁN {GlobalUtil.numberWithCommas(cartData?.grandTotal)} ₫
          </button>
          <div className="flex justify-center items-center w-[80%]">
            <Link to="/" className="hover:underline">
              Tiếp tục mua sắm
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
