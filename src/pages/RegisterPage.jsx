import React from "react";
import { useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AuthService } from "../services/auth.service";
import Swal from "sweetalert2";

const Register = () => {
  const navigate = useNavigate();
  const fullNameRef = useRef(null);
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const confirmPasswordRef = useRef(null);
  const showErrorAlert = (message) => {
    Swal.fire({
      icon: "error",
      title: "Lỗi",
      text: message,
      footer:
        '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
    });
  };
  const submitHandler = async (e) => {
    e.preventDefault();
    const fullname = fullNameRef.current.value;
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    const confirmPassword = confirmPasswordRef.current.value;
    if (fullname.trim() === "" && password.trim() === "" && email.trim() === "") {
      showErrorAlert("Hãy nhập đầy đủ thông tin!");
      fullNameRef.current.focus();
    } else if (fullname.trim() === "") {
      showErrorAlert("Hãy nhập thông tin người dùng!");
      fullNameRef.current.focus();
    } else if (password.trim() === "") {
      showErrorAlert("Hãy nhập mật khẩu!");
      passwordRef.current.focus();
    } else if (email.trim() === "") {
      showErrorAlert("Hãy nhập số điện thoại!");
      email.current.focus();
    } else if (confirmPassword.trim() === "") {
      showErrorAlert("Hãy nhập mật khẩu xác nhận!");
      confirmPasswordRef.current.focus();
    } else if (password.trim() !== confirmPassword.trim()) {
      showErrorAlert("Mật khẩu xác nhận không đúng!");
      confirmPasswordRef.current.focus();
    } else {
      AuthService.register(fullname, email, password, confirmPassword).then((response) => {
        if (response.status === "OK") {
          navigate("/login");
        } else {
          showErrorAlert(response.message);
        }
      });
    }
  };
  return (
    <div className="flex justify-center items-start h-fit mt-[182px]">
      <div className="flex flex-col w-1/3 border border-gray-400 pb-[80px] gap-1 drop-shadow-xl bg-white pt-10">
        <label className="text-center h-9 mb-5 text-3xl font-semibold content-center w-full">
          ĐĂNG KÝ
        </label>
        <input
          name="fullName"
          placeholder=" Họ và tên"
          className="my-2 mx-auto w-[70%] h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
          ref={fullNameRef}
        />
        <input
          name="account"
          placeholder=" Email"
          className="my-2 mx-auto w-[70%] h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
          ref={emailRef}
        />
        <input
          name="password"
          type="password"
          placeholder=" Mật khẩu"
          className="my-2 mx-auto w-[70%] h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
          ref={passwordRef}
        />
        <input
          name="confirmPassword"
          type="password"
          placeholder=" Nhập lại mật khẩu"
          className="my-2 mx-auto w-[70%] h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
          ref={confirmPasswordRef}
        />

        <button
          type="button"
          onClick={submitHandler}
          className="w-[70%] h-[50px] mx-auto my-3 bg-orange-500  hover:bg-orange-600 text-white text-center focus:drop-shadow-sm"
        >
          Đăng ký
        </button>
        <div className="mx-auto">
          Bạn đã có tài khoản?{" "}
          <Link to="/login" className=" underline hover:text-orange-500">
            Đăng nhập
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Register;
