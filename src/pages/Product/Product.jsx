/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { ProductGallery } from "../../components";
import mainImage from "../../assets/HomePage1.png";
import { Suspense, useEffect } from "react";
import useProductFilter from "../../hooks/useProductFilter";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Accordion } from "flowbite-react";

const Product = (props) => {
  const {
    colors,
    categories,
    sizes,
    filterData,
    pageList,
    setMinPrice,
    setMaxPrice,
    setCategoryFilter,
    setSizeFilter,
    setColorFilter,
    page,
    setPage,
    isCategoryInit,
    isColorInit,
    isSizeInit,
    setCategoryInit,
    setColorInit,
    setSizeInit,
    setGender,
    setName,
    name,
    message,
    setMessage,
    sortOption,
    setSortOption,
  } = useProductFilter();
  const [requestParams, setRequestParams] = useSearchParams();
  const navigate = useNavigate();
  const sortOptionOnChange = (e) => {
    setSortOption(e.target.value);
  };
  useEffect(() => {
    setName("");
    setPage(1);
    setMessage("");
    if (props.type === "male") {
      setGender(["MALE", "UNISEX"]);
      setMessage("");
    }
    if (props.type === "female") {
      setGender(["FEMALE", "UNISEX"]);
      setMessage("");
    }
    if (props.type === "unisex") {
      setGender(["UNISEX"]);
    }
    if (props.type === "search") {
      setGender(["FEMALE", "UNISEX", "MALE"]);
      setName(String(requestParams.get("q")));
    }
    if (props.type === "all") {
      setGender(["FEMALE", "UNISEX", "MALE"]);
    }
    window.scrollTo(0, 0);
  }, [props.type, requestParams.get("q")]);
  return (
    <div className="flex flex-col justify-center items-center xl:mt-[71px] mt-[70px]">
      <img className="object-cover h-[200px] w-full" src={mainImage} alt="banner 1" id="banner" />
      {/* Sản phẩm nổi bật */}
      <div className="w-full flex justify-start items-start gap-2 mt-8 md:flex-nowrap flex-wrap">
        <div className="md:w-[18%] pl-10 pr-4 w-full">
          <div className="flex justify-start gap-2 items-center mb-[10px]">
            <select className="w-full" onChange={sortOptionOnChange}>
              <option value="newest" selected>
                Sản phẩm mới
              </option>
              <option value="price_desc">Giá giảm dần</option>
              <option value="price_asc">Giá tăng dần</option>
              <option value="name_asc">Theo tên A-Z</option>
              <option value="name_desc">Theo tên Z-A</option>
            </select>
          </div>
          <div className="border-t border-gray-300 pt-2">
            <Accordion alwaysOpen={true}>
              <Accordion.Panel alwaysOpen={true}>
                <Accordion.Title>DANH MỤC</Accordion.Title>
                <Accordion.Content>
                  <div className="flex flex-col gap-2">
                    {categories.map((item) => {
                      return (
                        <label key={item.id} className="flex gap-4 justify-start items-center">
                          <input
                            type="checkbox"
                            name="category"
                            value={item.id}
                            className="w-5 h-5 accent-black"
                            onChange={(e) => {
                              setCategoryFilter((prev) => {
                                if (isCategoryInit) {
                                  setCategoryInit(false);
                                  if (e.target.checked) {
                                    return [Number(e.target.value)];
                                  } else {
                                    return prev.filter((e) => {
                                      return e !== item.id;
                                    });
                                  }
                                } else {
                                  if (e.target.checked) {
                                    return [...prev, Number(e.target.value)];
                                  } else {
                                    return prev.filter((e) => {
                                      return e !== item.id;
                                    });
                                  }
                                }
                              });
                            }}
                          />
                          <p className="capitalize">{item.name}</p>
                        </label>
                      );
                    })}
                  </div>
                </Accordion.Content>
              </Accordion.Panel>
              <Accordion.Panel>
                <Accordion.Title>MÀU SẮC</Accordion.Title>
                <Accordion.Content>
                  <div className="flex flex-col gap-2">
                    {colors.map((item) => {
                      return (
                        <label key={item.id} className="flex gap-4 justify-start items-start">
                          <input
                            type="checkbox"
                            name="category"
                            value={item.id}
                            className="w-5 h-5 accent-black mt-[2px]"
                            onChange={(e) => {
                              setColorFilter((prev) => {
                                if (isColorInit) {
                                  setColorInit(false);
                                  if (e.target.checked) {
                                    return [Number(e.target.value)];
                                  } else {
                                    return prev.filter((e) => {
                                      return e !== item.id;
                                    });
                                  }
                                } else {
                                  if (e.target.checked) {
                                    return [...prev, Number(e.target.value)];
                                  } else {
                                    return prev.filter((e) => {
                                      return e !== item.id;
                                    });
                                  }
                                }
                              });
                            }}
                          />
                          <span
                            className="z-[1000] w-5 h-5  rounded-sm mt-[2px]"
                            style={{ backgroundColor: item.code }}
                          ></span>
                          <p className="uppercase">{item.name}</p>
                        </label>
                      );
                    })}
                  </div>
                </Accordion.Content>
              </Accordion.Panel>
              <Accordion.Panel>
                <Accordion.Title>KÍCH THƯỚC</Accordion.Title>
                <Accordion.Content>
                  <div className="flex flex-col gap-2">
                    {sizes.map((item) => {
                      return (
                        <label key={item.id} className="flex gap-4 justify-start items-start">
                          <input
                            type="checkbox"
                            name="category"
                            value={item.id}
                            className="w-5 h-5 accent-black mt-[2px]"
                            onChange={(e) => {
                              setSizeFilter((prev) => {
                                if (isSizeInit) {
                                  setSizeInit(false);
                                  if (e.target.checked) {
                                    return [Number(e.target.value)];
                                  } else {
                                    return prev.filter((e) => {
                                      return e !== item.id;
                                    });
                                  }
                                } else {
                                  if (e.target.checked) {
                                    return [...prev, Number(e.target.value)];
                                  } else {
                                    return prev.filter((e) => {
                                      return e !== item.id;
                                    });
                                  }
                                }
                              });
                            }}
                          />
                          <p className="uppercase">{item.name}</p>
                        </label>
                      );
                    })}
                  </div>
                </Accordion.Content>
              </Accordion.Panel>
              <Accordion.Panel>
                <Accordion.Title>
                  GIÁ TIỀN <span className="text-sm font-normal">(Đơn vị VNĐ)</span>
                </Accordion.Title>
                <Accordion.Content>
                  <label>
                    <h1>Từ</h1>
                    <input
                      className="w-full border rounded-none h-[40px] focus:outline-none px-3 border-gray-500 mt-1"
                      type="number"
                      name="price"
                      onChange={(e) => {
                        setMinPrice(Number(e.target.value));
                      }}
                      min={10000}
                      defaultValue={10000}
                    />
                  </label>
                  <label>
                    <h1>Đến</h1>
                    <input
                      className="w-full border rounded-none h-[40px] focus:outline-none px-3 border-gray-500 mt-1"
                      type="number"
                      name="price"
                      onChange={(e) => {
                        setMaxPrice(Number(e.target.value));
                      }}
                      min={10000}
                      defaultValue={10000000}
                    />
                  </label>
                </Accordion.Content>
              </Accordion.Panel>
            </Accordion>
          </div>
        </div>
        <div className="md:w-[82%] w-full">
          <div className="flex flex-wrap md:items-center md:justify-start w-full justify-start items-start my-3 gap-x-[10px] gap-y-[15px] ">
            <div className="text-center w-full">{message}</div>
            <div className="md:w-full w-[90%] flex flex-wrap md:items-center md:justify-start justify-start items-start my-6 gap-x-[10px] gap-y-[15px] mx-auto">
              <Suspense fallback={<div>Loading</div>}>
                {filterData?.data?.map((item, index) => (
                  <ProductGallery {...item} width={380} key={index} />
                ))}
              </Suspense>
            </div>
          </div>
          <div className="flex gap-3 justify-end pr-[60px]">
            {pageList.map((item, index) => {
              return (
                <div
                  key={index}
                  className={
                    page !== item
                      ? "flex justify-center items-center font-semibold text-black h-[40px] w-[40px] border border-gray-600 hover:text-white hover:bg-black transition-all cursor-pointer"
                      : "flex justify-center items-center font-semibold  h-[40px] w-[40px] border border-gray-600 text-white bg-black transition-all cursor-pointer"
                  }
                  onClick={() => {
                    setPage(item);
                  }}
                >
                  {item}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
