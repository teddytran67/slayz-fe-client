/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useContext, useEffect, useRef } from "react";
import { AiOutlineUser } from "@react-icons/all-files/ai/AiOutlineUser";
import { GoLocation } from "@react-icons/all-files/go/GoLocation";
import { AddressItem } from "../../components";
import AddressForm from "../Account/AddressForm";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import useLocationForm from "../Account/useLocationForm";
import { useState } from "react";
import Select from "react-select";
import { IoMdArrowBack } from "@react-icons/all-files/io/IoMdArrowBack";
import { AiFillCreditCard } from "@react-icons/all-files/ai/AiFillCreditCard";
import { AuthContext } from "../../contexts/AuthContext";
import { AddressService } from "../../services/address.service";

const CheckoutAddress = () => {
  const { state, onCitySelect, onDistrictSelect, onWardSelect } = useLocationForm(false, null);
  const user = JSON.parse(localStorage.getItem("user"))?.data?.userInfo;
  const navigate = useNavigate();
  const [province, setProvince] = useState();
  const [district, setDistrict] = useState();
  const [ward, setWard] = useState();
  const addressLine = useRef();
  const fullName = useRef();
  const phone = useRef();
  const [addressList, setAddressList] = useState([]);
  const [addressId, setAddressId] = useState(null);
  const {
    cityOptions,
    districtOptions,
    wardOptions,
    selectedCity,
    selectedDistrict,
    selectedWard,
  } = state;
  const {
    loadUser,
    authState: { isAuthenticated },
  } = useContext(AuthContext);

  const chooseAddressClickHandle = () => {
    if (addressId === null) {
      alert("Hãy chọn địa chỉ!");
    } else {
      AddressService.getAddressById(addressId).then((response) => {
        if (response.status === "OK") {
          localStorage.setItem("address", JSON.stringify(response.data));
          navigate("/checkout");
        } else {
          navigate("/login");
        }
      });
    }
  };

  const createAddressAndChooseClickHandle = () => {
    const createData = {
      fullName: fullName.current.value,
      phone: phone.current.value,
      addressLine: addressLine.current.value,
      province: province?.label ? province?.label : selectedCity?.label,
      district: district?.label ? district?.label : selectedDistrict?.label,
      ward: ward?.label ? ward?.label : selectedWard?.label,
      provinceId: province?.value ? province?.value : selectedCity?.value,
      districtId: district?.value ? district?.value : selectedDistrict?.value,
      wardId: ward?.value ? ward?.value : selectedWard?.value,
      userId: user.id,
    };
    if (validate(createData)) {
      AddressService.createAddress(createData).then((response) => {
        if (response.status === "OK") {
          localStorage.setItem("address", JSON.stringify(response.data));
          navigate("/checkout");
        }
      });
    }
  };
  const validate = (obj) => {
    if (obj.province === undefined) {
      alert("Hãy chọn tỉnh/thành phố!");
      return false;
    }
    if (obj.district === undefined) {
      alert("Hãy chọn quận/huyện!");
      return false;
    }
    if (obj.ward === undefined) {
      alert("Hãy chọn xã/phường");
      return false;
    }
    if (obj.addressLine.trim() === "") {
      alert("Hãy nhập đầy đủ thông tin địa chỉ!");
      addressLine.current.focus();
      return false;
    }
    if (obj.fullName.trim() === "") {
      alert("Hãy nhập họ tên đầy đủ!");
      fullName.current.focus();
      return false;
    }
    if (!obj.phone.match(/^\d{10}$/)) {
      alert("Số điện thoại hợp lệ, vui lòng nhập lại!");
      return false;
    }
    return true;
  };

  useEffect(() => {
    AddressService.getAllAddressByUserId(user.id).then((response) => {
      if (response.status === "OK") {
        setAddressList(response.data);
      } else {
        navigate("/login");
      }
    });
  }, []);
  if (loadUser() === false) {
    return <Navigate to="/login" />;
  }
  return (
    <div className="h-fit w-[80%] flex justify-start  items-center flex-col mx-auto mt-[102px]">
      <div className="flex justify-between items-center py-9 w-full">
        <div
          className="w-[20%] flex gap-3 justify-start items-center absolute my-auto "
          onClick={() => {
            window.scrollTo(0, 0);
            navigate("/user/cart");
          }}
        >
          <IoMdArrowBack className="cursor-pointer" />
          <p className="font-semibold hover:underline cursor-pointer">Trở về giỏ hàng</p>
        </div>
        <div className="w-full">
          <div className="w-[50%] flex relative justify-between mx-auto">
            <div className="border border-gray-400 w-[90%] h-[1px] absolute z-[-9999] mt-[19px] ml-[30px]"></div>
            <div className="flex flex-col justify-center items-center w-fit">
              <div className="rounded-full bg-orange-600 text-white w-10 h-10 flex justify-center items-center font-bold">
                1
              </div>
              <div className="font-semibold">Đăng nhập</div>
            </div>
            <div className="flex flex-col justify-center items-center w-fit">
              <div className="rounded-full bg-orange-600 text-white w-10 h-10 flex justify-center items-center font-bold">
                2
              </div>
              <div className="font-semibold">Địa chỉ giao hàng</div>
            </div>
            <div className="flex flex-col justify-center items-center w-fit">
              <div className="rounded-full bg-gray-400 text-white w-10 h-10 flex justify-center items-center font-bold">
                3
              </div>
              <div className="font-semibold text-gray-400">Thanh toán</div>
            </div>
          </div>
        </div>
      </div>
      <div className="border-t border-gray-500 w-full flex gap-3 py-10">
        <div className="flex justify-center items-start mt-1">
          <AiOutlineUser className="text-[20px] font-bold" />
        </div>
        <div className="flex flex-col justify-start items-start">
          <p className="font-bold text-[20px]">Đăng nhập</p>
          <p>0388891635</p>
        </div>
      </div>
      <div className="border-t border-gray-500 w-full flex gap-3 py-10 flex-col">
        <div className="flex gap-3 mb-6">
          <div className="flex justify-center items-start mt-1">
            <GoLocation className="text-[20px] font-bold" />
          </div>
          <div className="flex flex-col justify-start items-start">
            <p className="font-bold text-[20px]">Địa chỉ giao hàng</p>
          </div>
        </div>
        <div className="flex gap-10 divide-x divide-gray-400">
          <div
            className="w-1/2 flex flex-col gap-3"
            onChange={(e) => {
              setAddressId(Number(e.target.value));
            }}
          >
            {addressList.map((item) => {
              return (
                <label className="flex gap-3" key={item.id}>
                  <input type="radio" name="address" className="w-[30px]" value={item.id} />{" "}
                  <AddressItem {...item} />
                </label>
              );
            })}
            {addressList.length > 0 && (
              <button
                type="button"
                className="inline-flex justify-center border border-transparent bg-orange-500 py-3 px-[70px] text-sm font-medium text-white shadow-sm hover:bg-orange-600 focus:outline-none  w-full"
                onClick={chooseAddressClickHandle}
              >
                GIAO ĐẾN ĐỊA CHỈ ĐÃ CHỌN
              </button>
            )}
          </div>
          <div className="w-1/2 pl-[30px]">
            <div>
              <div className="pb-2 flex flex-col gap-2 ">
                <div>
                  <label className="block text-sm font-medium text-gray-700 w-full">
                    <h1 className="font-bold mb-1 text-[18px]">Tỉnh/Thành phố</h1>
                    <div className="inline-block relative w-full">
                      <Select
                        name="cityId"
                        key={`cityId_${selectedCity?.value}`}
                        isDisabled={cityOptions.length === 0}
                        options={cityOptions}
                        onChange={(option) => {
                          setProvince(option);
                          onCitySelect(option);
                        }}
                        placeholder="Tỉnh/Thành"
                        defaultValue={selectedCity}
                        className={`mt-1 w-full focus:outline-none text-[15px] border-gray-300 shadow-sm sm:text-[15px] `}
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700 w-full">
                    <h1 className="font-bold mb-1 text-[18px]">Quận/Huyện</h1>
                    <div className="inline-block relative w-full">
                      <Select
                        name="districtId"
                        key={`districtId_${selectedDistrict?.value}`}
                        isDisabled={districtOptions.length === 0}
                        options={districtOptions}
                        onChange={(option) => {
                          setDistrict(option);
                          onDistrictSelect(option);
                        }}
                        placeholder="Quận/Huyện"
                        defaultValue={selectedDistrict}
                        className={`mt-1 w-full focus:outline-none text-[15px] border-gray-300 shadow-sm sm:text-[15px] `}
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700 w-full">
                    <h1 className="font-bold mb-1 text-[18px]">Xã/Phường</h1>
                    <div className="inline-block relative w-full">
                      <Select
                        name="wardId"
                        key={`wardId_${selectedWard?.value}`}
                        isDisabled={wardOptions.length === 0}
                        options={wardOptions}
                        placeholder="Phường/Xã"
                        onChange={(option) => {
                          setWard(option);
                          onWardSelect(option);
                        }}
                        defaultValue={selectedWard}
                        classNamePrefix="react-select"
                        className={`react-select-container mt-1 w-full focus:outline-none border-gray-300 shadow-sm text-[15px] `}
                        size="5"
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700">
                    <h1 className="font-bold mb-1 text-[18px]">Địa chỉ</h1>
                    <div className="mt-1">
                      <input
                        id="address"
                        name="address"
                        type="text"
                        ref={addressLine}
                        className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                        placeholder="Ví dụ: Số 1, Đường Võ Văn Ngân"
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700">
                    <h1 className="font-bold mb-1 text-[18px]">Họ và tên</h1>
                    <div className="mt-1">
                      <input
                        id="name"
                        name="name"
                        type="text"
                        ref={fullName}
                        className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                        placeholder="Ví dụ: Nguyễn Văn A"
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700">
                    <h1 className="font-bold mb-1 text-[18px]">Số điện thoại</h1>
                    <div className="mt-1">
                      <input
                        id="phone"
                        name="phone"
                        type="tel"
                        ref={phone}
                        className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                        placeholder="Ví dụ: 0388 891 635"
                      />
                    </div>
                  </label>
                </div>
              </div>
              <div className="flex justify-start gap-2 py-3 text-right">
                <button
                  type="button"
                  className="inline-flex justify-center border border-transparent bg-orange-500 py-3 px-[70px] text-sm font-medium text-white shadow-sm hover:bg-orange-600 focus:outline-nones w-full"
                  onClick={createAddressAndChooseClickHandle}
                >
                  XÁC NHẬN GIAO ĐẾN ĐỊA CHỈ NÀY
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="border-y border-gray-500 w-full flex gap-3 py-10 text-gray-400">
        <div className="flex justify-center items-start mt-1">
          <AiFillCreditCard className="text-[20px] font-bold" />
        </div>
        <div className="flex flex-col justify-start items-start">
          <p className="font-bold text-[20px]">Thanh toán</p>
        </div>
      </div>
    </div>
  );
};

export default CheckoutAddress;
