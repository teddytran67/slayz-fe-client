/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { AiOutlineShoppingCart } from "@react-icons/all-files/ai/AiOutlineShoppingCart";
import { BiErrorCircle } from "@react-icons/all-files/bi/BiErrorCircle";
import { AiOutlineFileDone } from "@react-icons/all-files/ai/AiOutlineFileDone";
import React from "react";
import { useEffect } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { InvoiceService } from "../../services/invoice.service";

const ResultCheckOut = () => {
  const navigate = useNavigate();
  const [requestParams, setRequestParams] = useSearchParams();
  window.scrollTo(0, 0);
  useEffect(() => {
    if (requestParams.get("extraData") !== null) {
      if (Number(requestParams.get("resultCode")) === 0) {
        const invoiceId = Number(requestParams.get("extraData"));
        InvoiceService.updatePaymentStatus(invoiceId, { status: "PAID" }).then((response) => {
          if (response.status === "OK") {
            setTimeout(navigate("/order_result"), 3000);
          }
        });
      }
    }
  }, []);
  if (Number(requestParams.get("resultCode")) !== 0) {
    return (
      <div className="mt-[100px] h-[64vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
        <div>
          <BiErrorCircle className="text-[180px]" />
        </div>
        <div className="font-bold uppercase text-orange-500 text-[30px]">thanh toán thất bại</div>
        <div>Giao dịch chưa được xử lý, quý khách vui lòng tiến hành thanh toán lại!</div>
        <Link to="/" className="hover:underline">
          Quay về trang chủ, tiếp tục mua sắm
        </Link>
      </div>
    );
  }
  return (
    <div className="mt-[100px] h-[64vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
      <div>
        <AiOutlineFileDone className="text-[180px]" />
      </div>
      <div className="font-bold uppercase text-orange-500 text-[30px]">thanh toán thành công</div>
      <div>Xin chân thành cảm ơn quý khách hàng đã tin tưởng SlayZ</div>
      <Link to="/" className="hover:underline">
        Quay về trang chủ, tiếp tục mua sắm
      </Link>
    </div>
  );
};

export default ResultCheckOut;
