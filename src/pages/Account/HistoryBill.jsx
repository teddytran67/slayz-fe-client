/* eslint-disable no-unused-vars */
import React from "react";
import { Invoice } from "../../components";
import useInvoiceData from "../../hooks/useInvoiceData";
import { useNavigate } from "react-router-dom";
import { AiOutlineShoppingCart } from "@react-icons/all-files/ai/AiOutlineShoppingCart";

const HistoryBill = () => {
  const { invoiceList } = useInvoiceData();
  const navigate = useNavigate();

  if (invoiceList.length === 0) {
    return (
      <div className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-1 md:mt-[102px] px-4 md:px-0">
        <div className="py-5 border-b border-b-gray-200 w-full">
          <p className="font-bold text-[20px] uppercase">LỊCH SỬ MUA HÀNG</p>
          <p className="text-[14px]">Bạn có thể xem lịch sử mua hàng của SlayZ tại đây.</p>
        </div>
        <div className="flex flex-col gap-6 pt-[90px] justify-center items-center">
          <div>
            <AiOutlineShoppingCart className="text-[140px]" />
          </div>
          <div>
            Bạn chưa mua sản phẩm nào của <span className="font-semibold">SlayZ</span>
          </div>
          <button
            className="transition-all duration-200 hover:shadow-md bg-none p-3 w-[30%] text-black border border-gray-600 hover:bg-black hover:text-white"
            type="button"
            onClick={() => {
              navigate("/");
            }}
          >
            QUAY LẠI MUA SẮM
          </button>
        </div>
      </div>
    );
  }
  return (
    <div className="flex flex-col gap-3 w-2/3 h-fit mb-[300px] mt-[102px]">
      <div className="py-5 border-b border-b-gray-200 w-full">
        <p className="font-bold text-[20px] uppercase">Lịch sử mua hàng</p>
        <p className="text-[14px]">Bạn có thể xem lịch sử mua hàng của SlayZ tại đây.</p>
      </div>
      <div className="flex flex-col gap-6 mt-4">
        {invoiceList.map((item) => {
          return <Invoice {...item} key={item?.id} />;
        })}
      </div>
    </div>
  );
};

export default HistoryBill;
