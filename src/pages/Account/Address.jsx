/* eslint-disable no-unused-vars */
import React from "react";
import { useNavigate } from "react-router-dom";
import { AddressItem } from "../../components";
import { useDataContext } from "../../contexts/DataProvider";
import useAddressUser from "../../hooks/useAddressUser";

const Address = () => {
  const navigate = useNavigate();
  const { addressList } = useDataContext();
  const { address } = useAddressUser(null);
  return (
    <div className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-1 md:mt-[102px] px-4 md:px-0">
      <div className="py-5 border-b border-b-gray-200 w-full">
        <p className="font-bold text-[20px]">DANH SÁCH ĐỊA CHỈ GIAO HÀNG</p>
        <p className="text-[14px]">Bạn có thể xem và chỉnh sửa địa chỉ giao hàng tại đây.</p>
      </div>
      <div className="flex flex-col gap-6 mt-4">
        {addressList?.map((address) => {
          return <AddressItem {...address} key={address.id} />;
        })}
      </div>
      <div>
        <button
          type="button"
          className="w-full py-4 bg-main-bg text-white hover:bg-hover-main-bg mt-2"
          onClick={() => {
            navigate("/account/address/add");
          }}
        >
          THÊM ĐỊA CHỈ MỚI
        </button>
      </div>
    </div>
  );
};

export default Address;
