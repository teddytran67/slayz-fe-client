/* eslint-disable no-unused-vars */
import React, { useContext } from "react";
import { useState } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import Swal from "sweetalert2";

const Account = () => {
  const {
    authState: { isAuthenticated },
    logoutUser,
    loadUser,
  } = useContext(AuthContext);

  const navigate = useNavigate();

  const logout = () => {
    Swal.fire({
      title: "Đăng xuất",
      text: "Bạn có muốn đăng xuất tài khoản không?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#ff4949",
      cancelButtonColor: "#a0a0a0",
      cancelButtonText: "HỦY BỎ",
      confirmButtonText: "ĐĂNG XUẤT",
    }).then((result) => {
      if (result.isConfirmed) {
        logoutUser();
        navigate("/login");
      }
    });
  };

  if (loadUser() === false) {
    return <Navigate to="/login" />;
  }

  return (
    <div className="h-fit w-full md:w-1/3 mx-auto flex justify-start items-start mb-10 md:mb-[100px] mt-[122px] pl-0 md:pl-[80px]">
      <div className="w-full flex flex-col gap-4 justify-center items-center md:justify-center md:items-start">
        <p className="font-bold mb-6 text-[20px]">TÀI KHOẢN CỦA BẠN</p>
        <Link
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg"
          to="/account"
        >
          THÔNG TIN TÀI KHOẢN
        </Link>
        <Link
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg"
          to="/account/bill"
        >
          LỊCH SỬ MUA HÀNG
        </Link>
        <Link
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg"
          to="/account/address"
        >
          DANH SÁCH ĐỊA CHỈ
        </Link>
        <div
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg"
          onClick={logout}
        >
          ĐĂNG XUẤT
        </div>
      </div>
    </div>
  );
};

export default Account;
