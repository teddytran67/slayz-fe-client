import "../index.css";
import { useState, useRef, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiLockClosed } from "@react-icons/all-files/hi/HiLockClosed";
import { HiEyeOff } from "@react-icons/all-files/hi/HiEyeOff";
import { HiEye } from "@react-icons/all-files/hi/HiEye";
import { AuthContext } from "../contexts/AuthContext";
import React from "react";

const Login = () => {
  const { loginUser } = useContext(AuthContext);
  const isRemember = useRef();
  const navigate = useNavigate();
  const [isHiddenPassword, setIsHiddenPassword] = useState(true);
  // const [usernameCheck, setUsernameCheck] = useState(false);
  // const [passwordCheck, setPasswordCheck] = useState(false);
  const [falseloginCheck, setFalseLoginCheck] = useState(false);
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);
  const hidePasswordClick = () => {
    setIsHiddenPassword(!isHiddenPassword);
  };

  const keyEnter = (e) => {
    if (e.keyCode === 13) {
      submitHandler();
    }
  };

  const submitHandler = async () => {
    if (isRemember.current.checked === false) {
      window.addEventListener("beforeunload", () => localStorage.removeItem("user"));
    }
    const username = usernameRef.current.value;
    const password = passwordRef.current.value;
    if (username.trim() === "" && password.trim() === "") {
      usernameRef.current.focus();
    } else if (username.trim() === "") {
      usernameRef.current.focus();
    } else if (password.trim() === "") {
      passwordRef.current.focus();
    } else {
      try {
        const response = await loginUser(username.trim(), password);
        if (response && response.success) {
          setFalseLoginCheck(false);
          navigate("/");
        }
        setFalseLoginCheck(true);
      } catch (error) {
        console.log(error);
      }
    }
  };
  window.scrollTo(0, 0);
  return (
    <div className="flex justify-center items-start h-fit mt-[120px] xl:mt-[182px]">
      <div className="flex flex-col xl:w-[636px] border border-gray-400 pb-[80px] gap-1 drop-shadow-xl bg-white w-[336px]">
        <label className="mx-auto text-center h-fit mb-10 text-3xl font-semibold content-center w-full px-3 pt-10">
          ĐĂNG NHẬP
        </label>
        <div className="w-[70%] mx-auto flex justify-center items-center">
          <div className="bg-black h-[50px] xl:w-[45px] w-[40px] flex justify-center items-center">
            <HiUser className="text-white text-[25px]" />
          </div>
          <input
            placeholder="Email hoặc số điện thoại"
            className="w-full my-2 h-[50px] hover:border-primary-color border-2 border-black focus:outline-none px-3 focus:drop-shadow-md bg-white xl:placeholder:text-md placeholder:text-sm"
            ref={usernameRef}
            onKeyUp={keyEnter}
          />
        </div>
        <div className="w-[70%] mx-auto flex justify-center items-center">
          <div className="bg-black h-[50px] w-[50px] flex justify-center items-center">
            <HiLockClosed className="text-white text-[25px]" />
          </div>
          <input
            type={isHiddenPassword ? "password" : "text"}
            placeholder="Mật khẩu"
            className="w-full ring-0 focus:ring-0 my-2 h-[50px] hover:border-primary-color border-2 border-black focus:outline-none px-3 focus:drop-shadow-md bg-white xl:placeholder:text-md placeholder:text-sm"
            ref={passwordRef}
          />
          <div
            onClick={hidePasswordClick}
            className="bg-black h-[50px] w-[50px] flex justify-center items-center"
          >
            {isHiddenPassword ? (
              <HiEyeOff className="text-white text-[22px]" />
            ) : (
              <HiEye className="text-white text-[22px]" />
            )}
          </div>
        </div>

        <div className="w-[70%] mx-auto flex flex-col mt-1 xl:flex-row xl:justify-between justify-start items-start">
          <label className="w-fit flex xl:justify-center xl:items-center gap-2 justify-start items-start">
            <input
              type="checkbox"
              name=""
              id=""
              className="w-5 h-5 accent-black"
              defaultChecked={true}
              ref={isRemember}
            />
            Ghi nhớ đăng nhập
          </label>
          <Link to="/forgot_password" className="hover:underline w-fit">
            Quên mật khẩu?
          </Link>
        </div>
        {falseloginCheck && (
          <div className="h-6 grid justify-self-center">
            <label className="text-center">Tài khoản hoặc mật khẩu không đúng</label>
          </div>
        )}
        <button
          type="button"
          onClick={submitHandler}
          className="w-[70%] h-[50px] mx-auto my-3 bg-main-bg  hover:bg-hover-main-bg text-white text-center focus:drop-shadow-sm uppercase"
        >
          Đăng nhập
        </button>
        <div className="mx-auto w-[70%] flex flex-col justify-start items-center border-t pt-8 mt-4 border-gray-400">
          <button
            type="button"
            className="w-full h-[50px] mx-auto bg-black  hover:bg-gray-600 text-white text-center focus:drop-shadow-sm uppercase"
            onClick={() => {
              navigate("/register");
            }}
          >
            Đăng ký
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
