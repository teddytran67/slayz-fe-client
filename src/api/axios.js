import axios from "axios";

export var config = {
  baseURL: "http://localhost:8080",
  headers: {},
};

export const api = axios.create(config);
