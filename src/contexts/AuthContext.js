import { createContext, useEffect, useState } from "react";
import { api, config } from "../api/axios";
import { UserService } from "../services/user.service";
import Swal from "sweetalert2";

export const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {
  const [authState, setAuthState] = useState({
    isAuthenticated: false,
    user: JSON.parse(localStorage.getItem("user"))?.data?.info,
  });
  const [isAuthen, setAuthen] = useState(false);
  const loginUser = async (username, password) => {
    try {
      const data = { username, password };
      const response = await api.post("/api/v1/auth/login", data);
      if (response.data.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
        setAuthState({
          ...authState,
          isAuthenticated: true,
        });
        config.headers["Authorization"] = `Bearer ${
          JSON.parse(localStorage.getItem("user"))?.data?.accessToken
        }`;
        return {
          success: true,
          data: response.data,
        };
      } else {
        Swal.fire({
          icon: "error",
          title: "Đăng nhập",
          text: "Sai thông tin tài khoản hoặc mật khẩu!",
          footer:
            '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
        });
        return {
          success: false,
        };
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Lỗi",
        text: "Sai thông tin tài khoản hoặc mật khẩu!",
        footer:
          '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
      });
    }
  };

  const getCurrentUser = () => {
    UserService.getCurrentUser().then((response) => {
      if (response.status === "OK") {
        setAuthState({
          isAuthenticated: true,
          user: response.data?.principal,
        });
      } else {
        setAuthState({
          isAuthenticated: false,
          user: null,
        });
      }
    });
  };

  const logoutUser = () => {
    localStorage.removeItem("user");
    delete config.headers.Authorization;
    setAuthState({
      ...authState,
      isAuthenticated: false,
    });
  };
  const loadUser = () => {
    if (JSON.parse(localStorage.getItem("user"))?.data.accessToken === undefined) {
      return false;
    }
    return true;
  };

  const authContextData = {
    logoutUser,
    loginUser,
    authState,
    isAuthen,
    loadUser,
    setAuthen,
    getCurrentUser,
  };

  useEffect(() => {
    getCurrentUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return <AuthContext.Provider value={{ ...authContextData }}>{children}</AuthContext.Provider>;
};

export default AuthContextProvider;
