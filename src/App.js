/* eslint-disable no-unused-vars */
import {
  Login,
  Register,
  Home,
  ProductDetail,
  NotFound,
  Cart,
  Account,
  HistoryBill,
  AccountDetail,
  AddressForm,
  Checkout,
  CheckoutAddress,
  HistoryBillDetail,
  AddressEdit,
  OrderResult,
  ResultCheckOut,
  Product,
  ForgotPassword,
  ResultSendMail,
  ResetPassword,
  ReviewForm,
} from "./pages";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import "./App.css";
import { Footer, Header, SideBar } from "./components";
import Address from "./pages/Account/Address";

function App() {
  return (
    <>
      <BrowserRouter>
        {/* <div className="w-72 fixed sidebar bg-white z-[10000]">
          <SideBar />
        </div> */}
        <Header />

        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/" element={<Home />} />
          <Route path="/products">
            <Route index element={<Product type={"all"} />} />
            <Route path=":id" element={<ProductDetail />} />
            <Route path="search" element={<Product type={"search"} />} />
            <Route path="male" element={<Product type={"male"} />} />
            <Route path="female" element={<Product type={"female"} />} />
            <Route path="unisex" element={<Product type={"unisex"} />} />
          </Route>
          <Route path="/register" element={<Register />} />
          <Route path="/forgot_password" element={<ForgotPassword />} />
          <Route path="/send_mail_success" element={<ResultSendMail />} />
          <Route path="/reset_password" element={<ResetPassword />} />
          <Route path="/user/cart" element={<Cart />} />
          <Route path="/account">
            <Route
              index
              element={
                <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                  <Account />
                  <AccountDetail />
                </div>
              }
            />
            <Route path="review">
              <Route
                index
                element={
                  <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                    <Account />
                    <ReviewForm />
                  </div>
                }
              />
            </Route>
            <Route path="bill">
              <Route
                index
                element={
                  <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                    <Account />
                    <HistoryBill />
                  </div>
                }
              />
              <Route
                path=":id"
                element={
                  <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                    <Account />
                    <HistoryBillDetail />
                  </div>
                }
              />
            </Route>
            <Route path="address">
              <Route
                index
                element={
                  <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                    <Account />
                    <Address />
                  </div>
                }
              />
              <Route
                path="add"
                element={
                  <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                    <Account />
                    <AddressForm />
                  </div>
                }
              />
              <Route
                path=":id"
                element={
                  <div className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto">
                    <Account />
                    <AddressEdit />
                  </div>
                }
              />
            </Route>
          </Route>
          <Route path="/checkout/address" element={<CheckoutAddress />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/order_result" element={<OrderResult />} />
          <Route path="/checkout_result" element={<ResultCheckOut />} />
          <Route path="/*" element={<NotFound />} />
          <Route path="/not_found" element={<NotFound />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </>
  );
}

export default App;
