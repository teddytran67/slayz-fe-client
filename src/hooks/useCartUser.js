/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { CartService } from "../services/cart.service";
import { InvoiceService } from "../services/invoice.service";

const useCartUser = () => {
  const userInfo = JSON.parse(localStorage.getItem("user"))?.data?.userInfo;
  const [cartInfo, setCartInfo] = useState(null);
  const [cartDetailList, setCartDetailList] = useState([]);
  const checkOut = async (
    note,
    addressId,
    paymentMethod,
    paymentStatus,
    listProduct,
    deliveryFee,
    deliveryMethodName,
  ) => {
    const data = {
      userId: userInfo?.id,
      paymentMethod: paymentMethod,
      paymentStatus: paymentStatus,
      listDetailId: listProduct,
      note: note,
      addressId: addressId,
      deliveryFee: deliveryFee,
      deliveryMethodName: deliveryMethodName,
    };
    console.log(data);
    const fetch = await InvoiceService.checkOut(data);
    return fetch.data;
  };

  useEffect(() => {
    (async function () {
      const fetchCartId = await CartService.getCartByUserId(userInfo?.id);
      const cartData = await CartService.getCartById(fetchCartId?.data?.id);
      setCartInfo(cartData.data);
    })();
    (async function () {
      const fetchCartId = await CartService.getCartByUserId(userInfo?.id);
      const fetchListCartDetail = await CartService.getAllCartDetailByCartId(fetchCartId?.data?.id);
      setCartDetailList(fetchListCartDetail.data);
    })();
  }, []);

  return { cartInfo, cartDetailList, checkOut };
};

export default useCartUser;
