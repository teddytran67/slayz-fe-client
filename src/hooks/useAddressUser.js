/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { AddressService } from "../services/address.service";
import { useDataContext } from "../contexts/DataProvider";

const useAddressUser = (id) => {
  const userInfo = JSON.parse(localStorage.getItem("user"))?.data?.userInfo;
  const [address, setAddress] = useState();
  const { setAddressList } = useDataContext();
  const fetchAddessList = () => {
    AddressService.getAllAddressByUserId(userInfo?.id).then((response) => {
      if (response?.status === "OK") {
        setAddressList(response?.data);
      }
    });
  };
  const removeAddressById = (id) => {
    AddressService.removeAddressById(id).then((response) => {
      if (response.status === "OK") {
        fetchAddessList();
      }
    });
  };
  const getAddressById = () => {
    if (id !== null) {
      AddressService.getAddressById(id).then((response) => {
        if (response.status === "OK") {
          setAddress(response.data);
        }
      });
    }
  };

  useEffect(() => {
    fetchAddessList();
    getAddressById();
  }, []);
  return { fetchAddessList, removeAddressById, address };
};

export default useAddressUser;
