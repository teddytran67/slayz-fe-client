/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { CategoryService } from "../services/category.service";
import { ColorService } from "../services/color.service";
import { ProductService } from "../services/product.service";
import { SizeService } from "../services/size.service";
import { useSearchParams } from "react-router-dom";

const useProductFilter = () => {
  const limit = 8;
  const [requestParams, setRequestParams] = useSearchParams();
  const [filterData, setFilterData] = useState();
  const [categories, setCategories] = useState([]);
  const [colors, setColors] = useState([]);
  const [sizes, setSizes] = useState([]);
  const [categoryFilter, setCategoryFilter] = useState([]);
  const [colorFilter, setColorFilter] = useState([]);
  const [sizeFilter, setSizeFilter] = useState([]);
  const [minPrice, setMinPrice] = useState(10);
  const [maxPrice, setMaxPrice] = useState(2000000);
  const [page, setPage] = useState(1);
  const [pageList, setPageList] = useState([1]);
  const [isCategoryInit, setCategoryInit] = useState(true);
  const [isSizeInit, setSizeInit] = useState(true);
  const [isColorInit, setColorInit] = useState(true);
  const [name, setName] = useState("");
  const [gender, setGender] = useState(["MALE", "FEMALE", "UNISEX"]);
  const [message, setMessage] = useState("");
  const [sortOption, setSortOption] = useState("newest");
  const fetchData = () => {
    CategoryService.getAllCategory().then((response) => {
      setCategories(response.data);
    });
    ColorService.getAllColor().then((response) => {
      if (response.status === "OK") {
        setColors(response.data);
      }
    });
    SizeService.getAllSize().then((response) => {
      if (response.status === "OK") {
        setSizes(response.data);
      }
    });
  };

  const filterApplyOnClick = () => {
    if (categoryFilter.length === 0) {
      setCategoryInit(true);
      var categoryArr = [];
      for (let i = 0; i < categories.length; i++) {
        categoryArr.push(categories[i].id);
      }
      setCategoryFilter(() => {
        return [...categoryArr];
      });
    }
    if (colorFilter.length === 0) {
      setColorInit(true);
      var colorArr = [];
      for (let i = 0; i < colors.length; i++) {
        colorArr.push(colors[i].id);
      }
      setColorFilter(() => {
        return [...colorArr];
      });
    }
    if (sizeFilter.length === 0) {
      var sizeArr = [];
      for (let i = 0; i < sizes.length; i++) {
        sizeArr.push(sizes[i].id);
      }
      setSizeFilter(() => {
        return [...sizeArr];
      });
    }
    setPage(1);
    const data = {
      categories: categoryFilter,
      colors: colorFilter,
      limit: limit,
      maxPrice: maxPrice,
      minPrice: minPrice,
      name: name,
      offset: page,
      sizes: sizeFilter,
      gender: gender,
      sortOption: sortOption,
    };
    ProductService.filterData(data).then((response) => {
      if (response.status === "OK") {
        setFilterData(response.data);
        var pageCount = 0;
        if (response.data.totalRecord / limit > parseInt(response.data.totalRecord / limit)) {
          pageCount = parseInt(response.data.totalRecord / limit) + 1;
        } else {
          pageCount = parseInt(response.data.totalRecord / limit);
        }
        var pageArray = [];
        for (let i = 0; i < pageCount; i++) {
          pageArray.push(i + 1);
        }
        setPageList(pageArray);
        setPage(response.data.currentPage);
      }
    });
  };

  const pageOnChange = () => {
    if (categoryFilter.length === 0) {
      var categoryArr = [];
      for (let i = 0; i < categories.length; i++) {
        categoryArr.push(categories[i].id);
      }
      setCategoryFilter(() => {
        return [...categoryArr];
      });
    }
    if (colorFilter.length === 0) {
      var colorArr = [];
      for (let i = 0; i < colors.length; i++) {
        colorArr.push(colors[i].id);
      }
      setColorFilter(() => {
        return [...colorArr];
      });
    }
    if (sizeFilter.length === 0) {
      var sizeArr = [];
      for (let i = 0; i < sizes.length; i++) {
        sizeArr.push(sizes[i].id);
      }
      setSizeFilter(() => {
        return [...sizeArr];
      });
    }
    const data = {
      categories: categoryFilter,
      colors: colorFilter,
      limit: limit,
      maxPrice: maxPrice,
      minPrice: minPrice,
      name: name,
      offset: page,
      sizes: sizeFilter,
      gender: gender,
      sortOption: sortOption,
    };
    ProductService.filterData(data).then((response) => {
      if (response.status === "OK") {
        setFilterData(response.data);
        var pageCount = 0;
        if (response.data.totalRecord / limit > parseInt(response.data.totalRecord / limit)) {
          pageCount = parseInt(response.data.totalRecord / limit) + 1;
        } else {
          pageCount = parseInt(response.data.totalRecord / limit);
        }
        var pageArray = [];
        for (let i = 0; i < pageCount; i++) {
          pageArray.push(i + 1);
        }
        setPageList(pageArray);
        setPage(response.data.currentPage);
      }
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    });
  };

  useEffect(() => {
    pageOnChange();
  }, [page]);

  useEffect(() => {
    filterApplyOnClick();
  }, [categoryFilter, colorFilter, sizeFilter, maxPrice, minPrice, name, gender, sortOption]);
  useEffect(() => {
    fetchData();
  }, []);
  return {
    colors,
    categories,
    sizes,
    filterData,
    pageList,
    minPrice,
    maxPrice,
    setMinPrice,
    setMaxPrice,
    setCategoryFilter,
    setSizeFilter,
    setColorFilter,
    page,
    setPage,
    isCategoryInit,
    isColorInit,
    isSizeInit,
    setCategoryInit,
    setColorInit,
    setSizeInit,
    name,
    setName,
    gender,
    setGender,
    message,
    setMessage,
    sortOption,
    setSortOption,
  };
};

export default useProductFilter;
