/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { InvoiceService } from "../services/invoice.service";

const useInvoiceData = (id) => {
  const userInfo = JSON.parse(localStorage.getItem("user"))?.data?.userInfo;
  const [invoice, setInvoice] = useState(null);
  const [invoiceList, setInvoiceList] = useState([]);
  const [invoicePendingList, setInvoicePendingList] = useState([]);
  const [invoiceInProgressList, setInvoiceInProgressList] = useState([]);
  const [invoiceCompletedList, setInvoiceCompletedList] = useState([]);
  const [invoiceCancelledList, setInvoiceCancelledList] = useState([]);

  const fetchInvoiceList = () => {
    InvoiceService.getInvoiceByUserId(userInfo?.id).then((response) => {
      if (response.status === "OK") {
        setInvoiceList(response.data);
      }
    });
    InvoiceService.getAllInvoiceByStatus("PENDING").then((response) => {
      if (response.status === "OK") {
        setInvoicePendingList(response.data);
      }
    });
    InvoiceService.getAllInvoiceByStatus("IN PROGRESS").then((response) => {
      if (response.status === "OK") {
        setInvoiceInProgressList(response.data);
      }
    });
    InvoiceService.getAllInvoiceByStatus("COMPLETED").then((response) => {
      if (response.status === "OK") {
        setInvoiceCompletedList(response.data);
      }
    });
    InvoiceService.getAllInvoiceByStatus("CANCELLED").then((response) => {
      if (response.status === "OK") {
        setInvoiceCancelledList(response.data);
      }
    });
  };

  const fetchInvoiceById = () => {
    InvoiceService.getInvoiceById(id).then((response) => {
      if (response.status === "OK") {
        setInvoice(response.data);
        console.log(response.data);
      }
    });
  };

  useEffect(() => {
    if (id !== undefined) {
      fetchInvoiceById();
    }
    fetchInvoiceList();
  }, []);

  return {
    invoice,
    invoiceList,
    setInvoiceList,
    fetchInvoiceList,
    invoicePendingList,
    invoiceCompletedList,
    invoiceCancelledList,
    invoiceInProgressList,
  };
};

export default useInvoiceData;
