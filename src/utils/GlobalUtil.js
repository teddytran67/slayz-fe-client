const numberWithCommas = (number) => {
  if (number === undefined || number === null) {
    return "0";
  }
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
const dateTimeConvert = (milliseconds) => {
  const date = new Date(milliseconds);
  return (
    date.getDate() +
    "-" +
    (date.getMonth() + 1) +
    "-" +
    date.getUTCFullYear() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes()
  );
};
export const GlobalUtil = {
  numberWithCommas,
  dateTimeConvert,
};
