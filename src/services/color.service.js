import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const getColorById = (id) => {
  return api.get("/api/v1/color/" + id).then((response) => {
    return response.data.data;
  });
};

const getAllColor = () => {
  return api.get("/api/v1/color", config).then((response) => {
    return response.data;
  });
};

export const ColorService = {
  getAllColor,
  getColorById,
};
