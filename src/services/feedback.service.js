import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${localStorage.getItem("token")}`;

const getFeedbackById = (id) => {
  return api.get("/api/v1/review/" + id).then((response) => {
    return response.data;
  });
};

const getAllFeedback = () => {
  return api.get("/api/v1/review", config).then((response) => {
    return response.data;
  });
};

const createNewReview = (data) => {
  return api.post("/api/v1/review", data, config).then((response) => {
    return response.data;
  });
};

const deleteReview = (id) => {
  return api.delete("/api/v1/review/" + id, config).then((response) => {
    return response.data;
  });
};

const disableReview = (id) => {
  return api.post(`/api/v1/review/${id}/disable`, null, config).then((response) => {
    return response.data;
  });
};

const enableReview = (id) => {
  return api.post(`/api/v1/review/${id}/enable`, null, config).then((response) => {
    return response.data;
  });
};

const getAllReviewByProductId = (payload) => {
  return api.post("/api/v1/review/product", payload, config).then((response) => {
    return response.data;
  });
};

const getAllReviewByUserId = (id) => {
  return api.get("/api/v1/review/user/" + id, config).then((response) => {
    return response.data;
  });
};

const getAvgRatingByProductId = (id) => {
  return api.get("/api/v1/review/avg_rating/" + id, config).then((response) => {
    return response.data;
  });
};

const getOverviewByProductId = (id) => {
  return api.get("/api/v1/review/overview_rating/" + id, config).then((response) => {
    return response.data;
  });
};

const getReviewByInvoiceDeatailId = (invoiceDetailId) => {
  return api.get("/api/v1/review/invoice_detail_id/" + invoiceDetailId, config).then((response) => {
    return response.data;
  });
};

export const FeedbackService = {
  getReviewByInvoiceDeatailId,
  getAllFeedback,
  createNewReview,
  deleteReview,
  disableReview,
  enableReview,
  getAllReviewByProductId,
  getAllReviewByUserId,
  getFeedbackById,
  getAvgRatingByProductId,
  getOverviewByProductId,
};
