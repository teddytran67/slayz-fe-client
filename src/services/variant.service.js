import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${localStorage.getItem("token")}`;

const getAllColorByProductId = (id) => {
	return api.get("/api/v1/variant/color?productId=" + id, config).then(
		(response) => {
			return response.data;
		},
		(error) => {
			return error;
		}
	);
};

const getAllSizeByProductId = (id) => {
	return api.get("/api/v1/variant/size?productId=" + id, config).then(
		(response) => {
			return response.data;
		},
		(error) => {
			return error;
		}
	);
};
const getSizeByProductIdAndColorId = (productId, colorId) => {
	return api
		.get("/api/v1/variant/size_chosen?productId=" + productId + "&colorId=" + colorId, config)
		.then(
			(response) => {
				return response.data;
			},
			(error) => {
				return error;
			}
		);
};

const getVariantByProductIdAndColorIdAndSizeId = (productId, colorId, sizeId) => {
	return api
		.get(
			"/api/v1/variant/find?productId=" + productId + "&colorId=" + colorId + "&sizeId=" + sizeId,
			config
		)
		.then(
			(response) => {
				return response.data;
			},
			(error) => {
				return error;
			}
		);
};

export const VariantService = {
	getAllColorByProductId,
	getAllSizeByProductId,
	getSizeByProductIdAndColorId,
	getVariantByProductIdAndColorIdAndSizeId,
};
