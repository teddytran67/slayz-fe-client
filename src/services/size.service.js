import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const getSizeById = (id) => {
  return api.get("/api/v1/size/" + id).then((response) => {
    return response.data.data;
  });
};

const getAllSize = () => {
  return api.get("/api/v1/size", config).then((response) => {
    return response.data;
  });
};

export const SizeService = {
  getAllSize,
  getSizeById,
};
