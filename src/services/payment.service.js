import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const createPayment = (data = {}) => {
  return api.post("/api/v1/payment/momo", data, config).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

export const PaymentService = {
  createPayment,
};
