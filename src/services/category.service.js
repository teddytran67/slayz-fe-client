import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const getAllCategory = () => {
  return api.get("/api/v1/category", config).then((response) => {
    return response.data;
  });
};

export const CategoryService = {
  getAllCategory,
};
