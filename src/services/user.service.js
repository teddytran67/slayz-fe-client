import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const getAllUser = () => {
  return api.get("/api/v1/user", config).then((response) => {
    return response.data;
  });
};

const getAllUserByStatus = (status) => {
  return api.get("/api/v1/user/status?status=" + status, config).then((response) => {
    return response.data;
  });
};

const getUserById = (id = 0) => {
  return api.get("/api/v1/user/" + id, config).then((response) => {
    return response.data;
  });
};

const updateUser = (id, data) => {
  return api.put("/api/v1/user/" + id, data, config).then((response) => {
    return response.data;
  });
};

const activeUser = (id) => {
  return api.patch("/api/v1/user/active/" + id, null, config).then((response) => {
    return response.data;
  });
};

const disableUser = (id) => {
  return api.patch("/api/v1/user/disable/" + id, null, config).then((response) => {
    return response.data;
  });
};

const updateRoleUser = (userId, roleId) => {
  return api.patch(`api/v1/user/${userId}/role/${roleId}`, null, config).then((response) => {
    return response.data;
  });
};

const updatePassword = (userId, data) => {
  return api.post(`api/v1/user/${userId}/password`, data, config).then((response) => {
    return response.data;
  });
};

const getCurrentUser = () => {
  return api.get("api/v1/user/current_user", config).then((response) => {
    return response.data;
  });
};

export const UserService = {
  getAllUser,
  getAllUserByStatus,
  getUserById,
  updateUser,
  activeUser,
  disableUser,
  updateRoleUser,
  updatePassword,
  getCurrentUser,
};
