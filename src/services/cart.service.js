import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
	JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const addProductToCart = (data = {}) => {
	return api.post("/api/v1/cart/add_to_cart", data, config).then(
		(response) => {
			return response.data;
		},
		(error) => {
			return error;
		}
	);
};

const getCartByUserId = (userId = 0) => {
	return api.get("/api/v1/cart/user/" + userId, config).then(
		(response) => {
			return response.data;
		},
		(error) => {
			return error;
		}
	);
};

const getAllCartDetailByCartId = (id = 0) => {
	return api.get("/api/v1/cart/cart_detail/" + id, config).then((response) => {
		return response.data;
	});
};

const getCartById = (id = 0) => {
	return api.get("/api/v1/cart/" + id, config).then((response) => {
		return response.data;
	});
};

const updateQuantity = (data = {}) => {
	return api.put("api/v1/cart/update_quantity", data, config).then((response) => {
		return response.data;
	});
};

const removeCartDetailFromCart = (id = 0) => {
	return api.delete("api/v1/cart/cart_detail/" + id, config).then((response) => {
		return response.data;
	});
};

export const CartService = {
	addProductToCart,
	getCartByUserId,
	getAllCartDetailByCartId,
	getCartById,
	updateQuantity,
	removeCartDetailFromCart,
};
