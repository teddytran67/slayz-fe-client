import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const createAddress = (data) => {
  return api.post("/api/v1/address", data, config).then((response) => {
    return response.data;
  });
};

const getAllAddress = () => {
  return api.get("/api/v1/address", config).then((response) => {
    return response.data;
  });
};

const updateAddress = (id, data) => {
  return api.put("/api/v1/address/" + id, data, config).then((response) => {
    return response.data;
  });
};

const getAddressById = (id) => {
  return api.get("/api/v1/address/" + id, config).then((response) => {
    return response.data;
  });
};

const setDefaultAddress = (id) => {
  return api.patch("/api/v1/address/" + id, config).then((response) => {
    return response.data;
  });
};

const getAllAddressByUserId = (id) => {
  return api.get("/api/v1/address/user/" + id, config).then((response) => {
    return response.data;
  });
};

const getDefaultAddressByUserId = (id) => {
  return api.get("/api/v1/address/default_address/" + id, config).then((response) => {
    return response.data;
  });
};

const removeAddressById = (id) => {
  return api
    .delete("/api/v1/address/" + id, config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log("Address delete error", error);
    });
};

export const AddressService = {
  getAllAddress,
  getAllAddressByUserId,
  getDefaultAddressByUserId,
  getAddressById,
  setDefaultAddress,
  updateAddress,
  createAddress,
  removeAddressById,
};
