import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const getAllProduct = () => {
  return api.get("/api/v1/product/product_grid", config).then((response) => {
    return response.data;
  });
};
const get12TheNewestProduct = () => {
  return api.get("/api/v1/product/newest_product", config).then((response) => {
    return response.data;
  });
};

const createProduct = (data) => {
  console.log("Create new product");
  return api.post("/api/v1/product", data, config).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

const updateProduct = (id, data) => {
  console.log("Update new product");
  return api.put("/api/v1/product/" + id, data, config).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

const getProductById = (id) => {
  return api.get("/api/v1/product/" + id, config).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

const getProductByBarcode = (barcode) => {
  return api.get("/api/v1/product/barcode/" + barcode, config).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

const deteleProductById = (id) => {
  return api.delete("/api/v1/product/" + id, config).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

const filterData = (data) => {
  return api.post("/api/v1/product/filter", data, config).then((response) => {
    return response.data;
  });
};
const getSaleQuantityByProductId = (id) => {
  return api.get("/api/v1/product/sale_quantity/" + id, config).then((response) => {
    return response.data;
  });
};

const getAllImageProductById = (id) => {
  return api.get("/api/v1/image_product/" + id).then((response) => {
    return response.data;
  });
};

const getRelatedProducts = (id) => {
  return api.get("/api/v1/product/" + id + "/related").then((response) => {
    return response.data;
  });
};

export const ProductService = {
  getRelatedProducts,
  getAllProduct,
  createProduct,
  updateProduct,
  deteleProductById,
  getProductById,
  getProductByBarcode,
  filterData,
  getSaleQuantityByProductId,
  get12TheNewestProduct,
  getAllImageProductById,
};
