import { api, config } from "../api/axios";

config.headers["Authorization"] = `Bearer ${
  JSON.parse(localStorage.getItem("user"))?.data?.accessToken
}`;

const checkOut = (data = {}) => {
  return api.post("/api/v1/invoice", data, config).then((response) => {
    return response.data;
  });
};

const updatePaymentStatus = (id, data = {}) => {
  return api.patch("/api/v1/invoice/payment_status/" + id, data, config).then((response) => {
    console.log(response);
    return response.data;
  });
};

const updateInvoiceStatus = (id, data = {}) => {
  return api.patch("/api/v1/invoice/status/" + id, data, config).then((response) => {
    console.log(response);
    return response.data;
  });
};

const getInvoiceByUserId = (userId) => {
  return api.get("/api/v1/invoice/users/" + userId, config).then((response) => {
    return response.data;
  });
};

const getAllInvoiceDetailByInvoiceId = (invoiceId) => {
  return api.get("/api/v1/invoice/invoice_detail/" + invoiceId, config).then((response) => {
    return response.data;
  });
};

const getInvoiceById = (id) => {
  return api.get("/api/v1/invoice/" + id, config).then((response) => {
    return response.data;
  });
};

const getAllInvoiceByStatus = (status) => {
  return api.get("/api/v1/invoice/status/" + status, config).then((response) => {
    return response.data;
  });
};

export const InvoiceService = {
  checkOut,
  updatePaymentStatus,
  getInvoiceByUserId,
  getAllInvoiceDetailByInvoiceId,
  getInvoiceById,
  updateInvoiceStatus,
  getAllInvoiceByStatus,
};
